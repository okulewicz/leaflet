function onMapClickCreator(mymap) {
    return function (e) {
        var popup = L.popup();
        popup
            .setLatLng(e.latlng)
            .setContent('<strong>Lokalizacja:</strong> ' +
                Math.round(e.latlng.lat * 1e6) / 1e6 + ', ' +
                Math.round(e.latlng.lng * 1e6) / 1e6)
            .openOn(mymap);
        console.info(e);
    };
}

function onMapPolygonCreator(mymap, colorpicker, resetter) {
    //aktualne współrzędne wierzchołków
    var latlngs = [];
    //obiekt Leafletowy reprezentujący wielokąt
    var polygon = L.polygon(latlngs);
    //resetter to przycisk "Zakończ"
    resetter.onclick = function () {
        //czyści tablicę, żeby zacząć rysowanie od nowa
        latlngs = [];
        polygon = L.polygon(latlngs);
    }
    return function (e) {
        //po każdym kliknięciu na mapie - nowy punkt do tablicy wierzchołków
        latlngs.push(e.latlng)
        console.info(latlngs);
        //usuwamy poprzednią wersję wielokąta z mapy
        polygon.removeFrom(mymap);
        //ustawiamy kolor z kontrolki kolorów jaki był przy kliknięciu
        polygon = L.polygon(latlngs, { color: colorpicker.value });
        polygon.addTo(mymap);
    };
}

function attachEventsToMap(mymap, showLatlngId, drawPolygonsId, colorpickerId, resetterId) {
    var myMapClickHandlerPopup = onMapClickCreator(mymap);
    var myMapClickHandlerPolygon = onMapPolygonCreator(
        mymap,
        document.getElementById(colorpickerId),
        document.getElementById(resetterId));
    document.getElementById(showLatlngId).addEventListener('change', function () {
        if (Boolean(this.checked)) {
            mymap.on('click', myMapClickHandlerPopup);
        } else {
            mymap.off('click', myMapClickHandlerPopup);
        }
    });
    document.getElementById(drawPolygonsId).addEventListener('change', function () {
        //jawna zamiana na typ logiczny, ponieważ w JS napise "false" jest prawdą
        //ponieważ jest niepusty (ale zmienna logiczna false jest fałszywa)
        if (Boolean(this.checked)) {
            mymap.on('click', myMapClickHandlerPolygon);
        } else {
            mymap.off('click', myMapClickHandlerPolygon);
        }
    });

}

function attachCustomWMSCreation(layersControl, buttonId, wmsLabelId, wmsAddressId, wmsLayersId, wmsFormatId) {
    document.getElementById(buttonId).addEventListener('click', function () {
        //Warunki zabudowy
        //http://miip.geomalopolska.pl/arcgis/services/MIIP_Plany_miejscowe_WMS/MapServer/WMSServer : 0
        //Hałas
        //http://miip.geomalopolska.pl/arcgis/services/WMS_Mapa_Akustyczna/MapServer/WMSServer : 1,13
        var wmsAddress = document.getElementById(wmsAddressId).value;
        var wmsLayers = document.getElementById(wmsLayersId).value;
        var wmsFormat = document.getElementById(wmsFormatId).value;
        var wmsLabel = document.getElementById(wmsLabelId).value;
        var customWMS = L.tileLayer.wms(wmsAddress, {
            layers: wmsLayers,
            format: wmsFormat,
            transparent: wmsFormat == 'image/png' ? true : false,
            attribution: wmsLabel,
            maxZoom: 21,
            minZoom: 3
        });

        layersControl.addOverlay(customWMS, wmsLabel);
    });
}

function attachCustomExtendedWMSCreation(layersControl, buttonId, wmsLabelId, wmsAddressId, wmsLayersId, wmsFormatId) {
    document.getElementById(buttonId).addEventListener('click', function () {
        var wmsAddress = document.getElementById(wmsAddressId).value;
        var wmsLayers = document.getElementById(wmsLayersId).value;
        var wmsFormat = document.getElementById(wmsFormatId).value;
        var wmsLabel = document.getElementById(wmsLabelId).value;
        var layersArray = wmsLayers.split(',');

        // https://github.com/heigeo/leaflet.wms
        // Recommended (explicit source)
        var options = {
            format: wmsFormat,
            transparent: wmsFormat == 'image/png' ? true : false,
            maxZoom: 21,
            minZoom: 3,
            attribution: wmsLabel
        };
        var wmsSource = L.WMS.source(wmsAddress, options);

        for (var i = 0; i < layersArray.length; ++i) {
            var wmsLayer = wmsSource.getLayer(layersArray[i]);
            layersControl.addOverlay(wmsLayer, wmsLabel + ': ' + layersArray[i]);
        }
    });
}

function loadLayers() {
    attachEventsToMap(
        mymap,
        'checkbox-show-latlng',
        'checkbox-draw-polygons',
        'colorpicker-draw-polygons',
        'reset-draw-polygons');

    var layersControl = L.control.layers(null, null, { 'collapsed': false });

    loadBaseLayers(layersControl);
    //loadCIOSOrtofoto(layersControl);
    //loadMiNIBuildingTiles(layersControl);
    //loadGDOSWMS(layersControl);
    //loadSagesKML(layersControl);
    //loadWFSCountriesAsGeoJSON(layersControl);
    attachCustomWMSCreation(layersControl,
        'add-wms-layer',
        'wms-layer-label',
        'wms-address',
        'wms-layers',
        'wms-format');

    attachCustomExtendedWMSCreation(layersControl,
        'add-wms-ext-layer',
        'wms-layer-label',
        'wms-address',
        'wms-layers',
        'wms-format');

    layersControl.addTo(mymap);

}

function loadWFSCountriesAsGeoJSON(layersControl) {
    fetch('https://ahocevar.com/geoserver/wfs?service=WFS&Request=GetFeature&version=1.1.0&TypeName=ne:ne_10m_admin_0_countries&outputFormat=application/json')
        //GeoJSON from WFS query
        .then(function (response) {
            return response.json();
        })
        .then(function (geojson) {
            var coorsLayer = L.geoJSON(geojson);
            layersControl.addOverlay(coorsLayer, 'ahocevar - kraje');
            coorsLayer.bindPopup(function (layer) {
                console.info('clicked marker');
                console.info(this);
                console.info(layer);
                return '<h4 class="popup-title">' + layer.feature.properties.name + '</h4>' +
                    '<p class="popup-content">' + layer.feature.properties.formal_en + '</p>';
            });
        });
}

function loadGDOSWMS(layersControl) {
    var corner1 = L.latLng(49, 18), corner2 = L.latLng(55, 25);
    var bounds = L.latLngBounds(corner1, corner2);
    //agregacja na poziomie WMSa
    var gdosWMSLayer = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszaryChronionegoKrajobrazu', 'GDOS:ObszarySpecjalnejOchrony'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    //agregacja na poziomie grupy warstw 
    var gdosWMSNatura2000Ock = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszaryChronionegoKrajobrazu'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    var gdosWMSNatura2000Oso = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszarySpecjalnejOchrony'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    var gdosWMSLayerGroup = L.layerGroup()
        .addLayer(gdosWMSNatura2000Ock)
        .addLayer(gdosWMSNatura2000Oso);

    layersControl.addOverlay(gdosWMSLayer, 'GDOS - pojedyczny WMS');
    layersControl.addOverlay(gdosWMSLayerGroup, 'GDOS - grupa');
    layersControl.addOverlay(gdosWMSNatura2000Ock, 'GDOS - obszary chronionego<br /> krajobrazu (Natura 2000)');
    layersControl.addOverlay(gdosWMSNatura2000Oso, 'GDOS - obszary specjalnej<br /> ochrony (Natura 2000)');

    L.featureGroup([gdosWMSLayer, gdosWMSLayerGroup, gdosWMSNatura2000Ock, gdosWMSNatura2000Oso])
        .eachLayer(function (layer) {
            layer.on('add', function () {
                mymap.flyToBounds(bounds);
            });
        });
}

function loadMiNIBuildingTiles(layersControl) {
    var miniLayers = L.layerGroup();
    var corner1 = L.latLng(52.2218, 21.0066), corner2 = L.latLng(52.2224, 21.0073);
    var bounds = L.latLngBounds(corner1, corner2);
    var miniBuilding0Layer = L.tileLayer('http://samorzad.mini.pw.edu.pl/plan2/images/Parter/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.mini.pw.edu.pl/">Wydzial MiNI PW</a>',
        minZoom: 17,
        maxZoom: 21,
        bounds: bounds
    });
    miniBuilding0Layer.addTo(mymap);
    layersControl.addOverlay(miniBuilding0Layer, 'MiNI PW - parter');
    miniLayers.addLayer(miniBuilding0Layer);
    /*
    Czy coś takiego ma prawo się udać?
    miniBuilding0Layer.on('add', function() {
        console.info('layer added');
        mymap.flyToBounds(miniBuilding0Layer.getBounds(), {maxZoom: mymap.getZoom()});
    })
    */
    miniBuilding0Layer.on('add', function () {
        console.info('layer added');
        mymap.flyToBounds(bounds);
    });
    //layersControl.addOverlay(miniLayers, 'MiNI PW');
    for (var floor = 1; floor <= 5; ++floor) {
        var miniBuildingFloorLayer = L.tileLayer('http://samorzad.mini.pw.edu.pl/plan2/images/Pietro' + floor + '/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="https://www.mini.pw.edu.pl/">Wydzial MiNI PW</a>',
            minZoom: 17,
            maxZoom: 21,
            bounds: bounds
        });
        layersControl.addOverlay(miniBuildingFloorLayer, 'MiNI PW - pietro ' + floor);
        miniLayers.addLayer(miniBuildingFloorLayer);
        miniBuildingFloorLayer.on('add', function () {
            console.info('layer added');
            mymap.flyToBounds(bounds);
        });
    }
}

function loadSagesKML(layersControl) {
    var kmlMarker = omnivore.kml('../data/sages.kml');
    kmlMarker.on('add', function () {
        console.info('layer added');
        mymap.flyToBounds(kmlMarker.getBounds(), { maxZoom: mymap.getZoom() });
    });
    kmlMarker.bindPopup(function (layer) {
        console.info('clicked marker');
        console.info(this);
        console.info(layer);
        return '<h4 class="popup-title">' + layer.feature.properties.name + '</h4>' +
            '<p class="popup-content">' + layer.feature.properties.description + '</p>';
    });
    layersControl.addOverlay(kmlMarker, 'Sages');
}

function loadCIOSOrtofoto(layersControl) {
    var ortofotomapa = L.tileLayer('http://mapa.ekoportal.pl/cache?x={x}&y={y}&z={z}&service=ortofotomapa&Layers=0', {
        attribution: 'Maps &amp; data &copy; <a href="http://mapa.ekportal.pl/">Ministerstwo Środowiska</a>,\
            contributors\
            ',
        minZoom: 9,
        maxZoom: 9,
        pane: 'overlayPane'
    });
    layersControl.addOverlay(ortofotomapa, 'Ortofotomapa');
}

function loadBaseLayers(layersControl) {
    var osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        pane: 'mapPane'
    });
    osmLayer.addTo(mymap);
    layersControl.addBaseLayer(osmLayer, '<img class="layer-selection-icon" src="../img/osm_logo.svg" alt="OSM Logo" />\
        Open Street Map');
    var cycleLayer = L.tileLayer('https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=808a4e5722614629a19573b8cd5332af', {
        attribution: 'Maps &copy; <a href="https://www.thunderforest.com/">Thunderforest</a>,\
        Data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 21,
        pane: 'mapPane'
    });
    layersControl.addBaseLayer(cycleLayer, '<img class="layer-selection-icon" src="../img/bicycle.svg" alt="bicycle icon" />\
        Cycling');
    var transportLayer = L.tileLayer('https://tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=808a4e5722614629a19573b8cd5332af', {
        attribution: 'Maps &copy; <a href="https://www.thunderforest.com/">Thunderforest</a>,\
        Data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 21,
        pane: 'mapPane'
    });
    layersControl.addBaseLayer(transportLayer, '<img class="layer-selection-icon" src="../img/transport.svg" alt="transport icon" />\
        Transport');
    var esriWorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
        pane: 'mapPane'
    });
    layersControl.addBaseLayer(esriWorldImagery, '<img class="layer-selection-icon" src="../img/esri.png" alt="transport icon" />\
        ESRI World Imagery');
}

function configureMap() {
    if (mymap.getCenter()) {
        loadLayers();
    } else {
        mymap.on('load', loadLayers);
    }
}

document.addEventListener("DOMContentLoaded", configureMap, false);