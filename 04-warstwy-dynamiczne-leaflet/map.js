var mymap;

function attachEvents(mymap) {
}

function setMapProperties(mymap) {
    mymap.setView([49.823809, 20.341187], 8);
}

function loadMap() {
    mymap = L.map('js-map-container');
    attachEvents(mymap);
    setMapProperties(mymap);
}

document.addEventListener( "DOMContentLoaded", loadMap, false );