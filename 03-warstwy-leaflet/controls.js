function format(input, leading, trailing) {
    if (Number(input.value) === 0) {
        input.value = '';
        for (var i = 0; i < leading; ++i) {
            input.value += '0';
        }
        if (trailing > 0) {
            input.value += '.';
            for (var i = 0; i < trailing; ++i) {
                input.value += '0';
            }
        }
        return;
    }
    var dotPos = input.value.indexOf('.');
    if (Number(trailing) > 0) {
        if (dotPos > 0) {
            var existing = input.value.length - dotPos - 1;
            for (var i = existing; i < trailing; ++i) {
                input.value += '0';
            }
        } else {
            input.value += '.'
            for (var i = 0; i < trailing; ++i) {
                input.value += '0';
            }
        }
    }
    if (Number(leading) > 0) {
        var missing = ((dotPos > 0) ? leading - dotPos : leading - input.value.length);
        for (var i = 0; i < missing; ++i) {
            input.value = '0' + input.value;
        }
    }
}

function formatControl() {
    format(this, this.getAttribute('data-leading'), this.getAttribute('data-trailing'));
}

function initializeControls() {
    var numericFields = document.getElementsByClassName('js-numeric-field');
    for (var i = 0; i < numericFields.length; ++i) {
        numericFields[i].addEventListener('change', formatControl, false);
    }
}

document.addEventListener("DOMContentLoaded", initializeControls, false);