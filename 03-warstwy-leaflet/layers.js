function loadLayers() {
    /*
    //alternatywne definicje warstw i kontrolek

        var baseLayers = {
        '<img class="layer-selection-icon" src="../img/osm_logo.svg" alt="OSM Logo" />\
            Open Street Map': osmLayer,
        '<img class="layer-selection-icon" src="../img/bicycle.svg" alt="bicycle icon" />\
            Cycling': cycleLayer,
        '<img class="layer-selection-icon" src="../img/transport.svg" alt="transport icon" />\
            Transport': transportLayer
    };

    var layersControl = L.control.layers(baseLayers, null, { 'collapsed': false }).addTo(mymap)

    var overlayLayers = {
        'MiNI PW - parter': miniBuilding0Layer,
        'MiNI PW - 1': miniBuilding1Layer,
        'MiNI PW - 2': miniBuilding2Layer,
        'MiNI PW - 3': miniBuilding3Layer,
        'MiNI PW - 4': miniBuilding4Layer,
        'MiNI PW - 5': miniBuilding5Layer
    };

    L.control.layers(baseLayers, overlayLayers, {'collapsed': false}).addTo(mymap);
*/

    var layersControl = L.control.layers(null, null, { 'collapsed': false });
    //layersControl.expand();

    loadBaseLayers(layersControl);
    loadCIOSOrtofoto(layersControl);
    loadMiNIBuildingTiles(layersControl);
    loadGDOSWMS(layersControl);
    loadSagesKML(layersControl);
    loadWFSCountriesAsGeoJSON(layersControl);

    layersControl.addTo(mymap);

}

function loadWFSCountriesAsGeoJSON(layersControl) {
    fetch('https://ahocevar.com/geoserver/wfs?service=WFS&Request=GetFeature&version=1.1.0&TypeName=ne:ne_10m_admin_0_countries&outputFormat=application/json')
        //GeoJSON from WFS query
        .then(function (response) {
            return response.json();
        })
        .then(function (geojson) {
            var coorsLayer = L.geoJSON(geojson);
            layersControl.addOverlay(coorsLayer, 'ahocevar - kraje');
            coorsLayer.bindPopup(function (layer) {
                console.info('clicked marker');
                console.info(this);
                console.info(layer);
                return '<h4 class="popup-title">' + layer.feature.properties.name + '</h4>' +
                    '<p class="popup-content">' + layer.feature.properties.formal_en + '</p>';
            });
        });
}

function loadGDOSWMS(layersControl) {
    var corner1 = L.latLng(49, 18), corner2 = L.latLng(55, 25);
    var bounds = L.latLngBounds(corner1, corner2);
    //agregacja na poziomie WMSa
    var gdosWMSLayer = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszaryChronionegoKrajobrazu', 'GDOS:ObszarySpecjalnejOchrony'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    //agregacja na poziomie grupy warstw 
    var gdosWMSNatura2000Ock = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszaryChronionegoKrajobrazu'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    var gdosWMSNatura2000Oso = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszarySpecjalnejOchrony'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    var gdosWMSLayerGroup = L.layerGroup()
        .addLayer(gdosWMSNatura2000Ock)
        .addLayer(gdosWMSNatura2000Oso);

    layersControl.addOverlay(gdosWMSLayer, 'GDOS - pojedyczny WMS');
    layersControl.addOverlay(gdosWMSLayerGroup, 'GDOS - grupa');
    layersControl.addOverlay(gdosWMSNatura2000Ock, 'GDOS - obszary chronionego<br /> krajobrazu (Natura 2000)');
    layersControl.addOverlay(gdosWMSNatura2000Oso, 'GDOS - obszary specjalnej<br /> ochrony (Natura 2000)');

    L.featureGroup([gdosWMSLayer, gdosWMSLayerGroup, gdosWMSNatura2000Ock, gdosWMSNatura2000Oso])
        .eachLayer(function (layer) {
            layer.on('add', function () {
            mymap.flyToBounds(bounds);
        });
    });
}

function loadMiNIBuildingTiles(layersControl) {
    var miniLayers = L.layerGroup();
    var corner1 = L.latLng(52.2218, 21.0066), corner2 = L.latLng(52.2224, 21.0073);
    var bounds = L.latLngBounds(corner1, corner2);
    var miniBuilding0Layer = L.tileLayer('http://samorzad.mini.pw.edu.pl/plan2/images/Parter/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.mini.pw.edu.pl/">Wydzial MiNI PW</a>',
        minZoom: 17,
        maxZoom: 21,
        //nie ma próby odczytu kafelków poza bounds
        bounds: bounds
    });
    miniBuilding0Layer.addTo(mymap);
    layersControl.addOverlay(miniBuilding0Layer, 'MiNI PW - parter');
    miniLayers.addLayer(miniBuilding0Layer);
    /*
    Czy coś takiego ma prawo się udać?
    miniBuilding0Layer.on('add', function() {
        console.info('layer added');
        mymap.flyToBounds(miniBuilding0Layer.getBounds(), {maxZoom: mymap.getZoom()});
    })
    */
    miniBuilding0Layer.on('add', function () {
        console.info('layer added');
        mymap.flyToBounds(bounds);
    });
    //layersControl.addOverlay(miniLayers, 'MiNI PW');
    for (var floor = 1; floor <= 5; ++floor) {
        var miniBuildingFloorLayer = L.tileLayer('http://samorzad.mini.pw.edu.pl/plan2/images/Pietro' + floor + '/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="https://www.mini.pw.edu.pl/">Wydzial MiNI PW</a>',
            minZoom: 17,
            maxZoom: 21,
            bounds: bounds
        });
        layersControl.addOverlay(miniBuildingFloorLayer, 'MiNI PW - pietro ' + floor);
        miniLayers.addLayer(miniBuildingFloorLayer);
        miniBuildingFloorLayer.on('add', function () {
            console.info('layer added');
            mymap.flyToBounds(bounds);
        });
    }
}

function loadSagesKML(layersControl) {
    //korzystamy z wtyczki omnivore
    var kmlMarker = omnivore.kml('../data/sages.kml');
    kmlMarker.on('add', function () {
        console.info('layer added');
        mymap.flyToBounds(kmlMarker.getBounds(), { maxZoom: mymap.getZoom() });
    });
    kmlMarker.bindPopup(function (layer) {
        console.info('clicked marker');
        console.info(this);
        console.info(layer);
        return '<h4 class="popup-title">' + layer.feature.properties.name + '</h4>' +
            '<p class="popup-content">' + layer.feature.properties.description + '</p>';
    });
    layersControl.addOverlay(kmlMarker, 'Sages');
}

function loadCIOSOrtofoto(layersControl) {
    var ortofotomapa = L.tileLayer('http://mapa.ekoportal.pl/cache?x={x}&y={y}&z={z}&service=ortofotomapa&Layers=0', {
        attribution: 'Maps &amp; data &copy; <a href="http://mapa.ekportal.pl/">Ministerstwo Środowiska</a>,\
            contributors\
            ',
        minZoom: 9,
        maxZoom: 9,
        pane: 'overlayPane'
    });
    layersControl.addOverlay(ortofotomapa, 'Ortofotomapa');
}

function loadBaseLayers(layersControl) {
    var osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        pane: 'mapPane'
    });
    osmLayer.addTo(mymap);
    layersControl.addBaseLayer(osmLayer, '<img class="layer-selection-icon" src="../img/osm_logo.svg" alt="OSM Logo" />\
        Open Street Map');
    var cycleLayer = L.tileLayer('https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=808a4e5722614629a19573b8cd5332af', {
        attribution: 'Maps &copy; <a href="https://www.thunderforest.com/">Thunderforest</a>,\
        Data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 21,
        pane: 'mapPane'
    });
    layersControl.addBaseLayer(cycleLayer, '<img class="layer-selection-icon" src="../img/bicycle.svg" alt="bicycle icon" />\
        Cycling');
    var transportLayer = L.tileLayer('https://tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=808a4e5722614629a19573b8cd5332af', {
        attribution: 'Maps &copy; <a href="https://www.thunderforest.com/">Thunderforest</a>,\
        Data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 21,
        pane: 'mapPane'
    });
    layersControl.addBaseLayer(transportLayer, '<img class="layer-selection-icon" src="../img/transport.svg" alt="transport icon" />\
        Transport');
    var esriWorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
        pane: 'mapPane'
    });
    layersControl.addBaseLayer(esriWorldImagery, '<img class="layer-selection-icon" src="../img/esri.png" alt="transport icon" />\
        ESRI World Imagery');
    //cycleLayer.addTo(mymap);
    //transportLayer.addTo(mymap);
    //ortofotomapa.addTo(mymap);
    /*
    Problems with loading correct attribution when loaded from control
    cycleLayer.addTo(mymap);
    transportLayer.addTo(mymap);
    */
}

function configureMap() {
    //function() {;}
    if (mymap.getCenter()) {
        loadLayers();
    } else {
        mymap.on('load', loadLayers);
    }
}

document.addEventListener("DOMContentLoaded", configureMap, false);