function loadMap() {
    var mymap = L.map('js-map-container').setView([50.05, 19.93], 11);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    maxZoom: 19,
    id: 'osm.tiles'
}).addTo(mymap);
}

document.addEventListener( "DOMContentLoaded", loadMap, false );
