//jeden obiekt dostępny we wszystkich skryptach
var miip = new Object();

function createStyledVectorLayer() {
    fetch("https://api.geomalopolska.pl/iMapLite/src/mirs.geojson")
        .then(function (data) {
            return data.json();
        })
        .then(function (geoJsonData) {

            //definicja kontrolki legendy objaśniającej mirs
            var legend = L.control({ position: 'bottomright' });

            legend.onAdd = function () {
                var div = L.DomUtil.create('div', 'info legend');
                div.innerHTML += '<h4>Inwestycje w zależności od wielkości</h4>'
                for (var kwota = 10000; kwota < 200000; kwota+=50000) {
                    var wielkosc = Math.round(kwota / 10000);
                    div.innerHTML +=
                    '<div><div style="float: left; width: ' + wielkosc +
                    'px; height: ' + wielkosc +
                     'px; border: 1px solid black; border-radius: ' +
                     Math.round(wielkosc / 2) +
                     'px;"></div>'+kwota+'</div>';
                }
                return div;
            };

            legend.addTo(miip.map);

            miip.map.on('moveend', function () {
                var picons = document.getElementsByClassName('mirs-p-icon');
                for (var i = 0; i < picons.length; ++i) {
                    if (Number(this.getZoom()) > 11) {
                        picons[i].style.display = 'block';
                    } else {
                        picons[i].style.display = 'none';
                    }
                    picons[i].style.textShadow = '0px 0px 4px #FFFFFF';

                }
            });
            //wykorzystanie wtyczki Fuse search
            miip.searchCtrl = L.control.fuseSearch();
            //dodanie kontrolki do mapy
            miip.searchCtrl.addTo(miip.map);

            var powiaty = {}

            for (var i = 0; i < geoJsonData.features.length; ++i) {
                var r = Math.round(Math.random() * 255);
                var g = Math.round(Math.random() * 255);
                var b = Math.round(Math.random() * 255);
                var colorForStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
                powiaty[geoJsonData.features[i].properties.POWIAT] = colorForStyle;
            }


            //wybranie pól po których będziemy wyszukiwać
            miip.searchCtrl.indexFeatures(geoJsonData, ['NAZWA_ZAD', 'KWOTA_DOF', 'MIEJSCOWOS', 'POWIAT', 'ROK', 'WNIOSKODAW']);
            miip.layers.mirsJsonLayer = L.geoJSON(geoJsonData,
                {
                    pointToLayer: function (feature, latLng) {
                        console.info(powiaty);
                        var color = 'black';
                        /*
                        if (feature.properties.ROK == '2018') {
                            color = 'red';
                        } else if (feature.properties.ROK == '2019') {
                            color = 'orange';
                        }
                        */
                        color = powiaty[feature.properties.POWIAT];
                        var circleMarkerOptions = {
                            radius: feature.properties.KWOTA_DOF / 10000,
                            color: color
                            //inne możliwe opcje stylizacji znacznika
                            /*,
                            fillOpacity: 1.0,
                            fillColor: 'black',
                            weight: 1*/
                        };
                
                        return L.circleMarker(latLng, circleMarkerOptions);
                    },
                    onEachFeature: function (feature, layer) {
                        //podpięcie wyskakujących okienek po kliknięciu na wyszukany obiekt
                        feature.layer = layer;
                        layer.bindPopup(createTextForMIRSPopup(feature));
                    }
                });
            miip.layers.mirsJsonLayer.bindPopup(function (layer) {
                //podpięcie wyskakujących okienek po kliknięciu na wyszukany obiekt
                var feature = layer.feature;
                return createTextForMIRSPopup(feature);
            });
            miip.layers.mirsJsonLayer.addTo(miip.map);
            miip.layerControl.addOverlay(miip.layers.mirsJsonLayer, 'MIRS');
        });

    function createTextForMIRSPopup(feature) {
        return '<h4 class="popup-title">' + feature.properties.MIEJSCOWOS +
            ' (' + feature.properties.DOFINANSOW + ')' + '</h4>' +
            '<p class="popup-content">' + feature.properties.NAZWA_ZAD + '</p>';
    }
}

function createBaseLayers() {
    var minZoom = 0;
    var maxZoom = 22;
    var switchZoom = 12;
    miip.layers.osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        minZoom: minZoom,
        maxZoom: switchZoom,
        pane: 'mapPane'
    });

    miip.layers.transportLayer = L.tileLayer('https://tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=808a4e5722614629a19573b8cd5332af', {
        attribution: 'Maps &copy; <a href="https://www.thunderforest.com/">Thunderforest</a>,\
        Data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        minZoom: switchZoom + 1,
        maxZoom: maxZoom,
        pane: 'mapPane'
    });

    miip.layers.baseMap = L.layerGroup([miip.layers.osmLayer, miip.layers.transportLayer],
        {
            minZoom: minZoom,
            maxZoom: maxZoom
        });

    miip.layers.baseMap.addTo(miip.map);
    miip.layerControl.addBaseLayer(miip.layers.baseMap, 'OSM');
}

function configureMapControls() {
    miip.map = L.map('js-map-container');
    miip.layerControl = L.control.layers(null, null/*, { 'collapsed': false }*/);
    miip.layerControl.addTo(miip.map);
    miip.layers = new Object();
    createBaseLayers();
    createStyledVectorLayer();
    if (!L.Browser.mobile) {
        miip.map.setView([52, 19.6], 6);
    } else {
        function onLocationFound(e) {
            var radius = e.accuracy / 2;

            document.getElementById('js-controls-container').innerHTML =
                "<p>Jesteś o " + radius + " metrów od punktu: " + e.latlng.lat + ', ' + e.latlng.lng + '</p>';

            L.circle(e.latlng, radius).addTo(miip.map);
        }

        function onLocationError(e) {
            document.getElementById('js-controls-container').innerHTML = '<p>' + e.message + '</p>';
        }

        miip.map.on('locationfound', onLocationFound);
        miip.map.on('locationerror', onLocationError);

        miip.map.locate({ setView: true, maxZoom: 16 });
    }
}

document.addEventListener("DOMContentLoaded", configureMapControls, false);