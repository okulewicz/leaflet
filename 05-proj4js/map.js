var mymap;

function attachEvents(mymap) {
}

function setMapProperties(mymap) {
    mymap.setView([49.823809, 20.341187], 8);
}

function loadMap() {
    proj4.defs("urn:ogc:def:crs:EPSG::26915", "+proj=utm +zone=15 +ellps=GRS80 +datum=NAD83 +units=m +no_defs");

    var geojson = {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [481650, 4980105]
        },
        crs: {
            type: "name",
            properties: {
                name: "urn:ogc:def:crs:EPSG::26915"
            }
        }
    };

    var mymap = L.map('js-map-container').setView([50.05, 19.93], 11);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
    <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    }).addTo(mymap);

    var layer = L.Proj.geoJson(geojson);
    layer.addTo(mymap);
    mymap.flyToBounds(layer.getBounds());

    /*
    mymap = L.map('js-map-container');
    attachEvents(mymap);
    setMapProperties(mymap);
    */
}

document.addEventListener("DOMContentLoaded", loadMap, false);