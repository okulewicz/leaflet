
function loadMap() {
    var mymap;
    var routingControl;

    mymap = L.map('js-map-container');
    //Należałoby dodać wtyczkę Proj4Leaflet i z nią eksperymentować
    //https://github.com/kartena/Proj4Leaflet
    //żeby dobrze ładować dane z MIIP_Orto2009, BDOT itd.
    var tileLayer = L.tileLayer(
        "http://miip.geomalopolska.pl/arcgis/\
rest/services/MIIP_Orto2009/MapServer/tile/{z}/{y}/{x}");
    //tileLayer.addTo(mymap);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mymap);

    L.Control.geocoder({
        defaultMarkGeocode: false
    })
        .on('markgeocode', function (e) {
            console.info(e);
            var bbox = e.geocode.bbox;
            var poly = L.polygon([
                bbox.getSouthEast(),
                bbox.getNorthEast(),
                bbox.getNorthWest(),
                bbox.getSouthWest()
            ]).addTo(mymap);
            mymap.fitBounds(poly.getBounds());
        })
        .addTo(mymap);
    mymap.fitWorld();

    mymap.on('click', function (e) {
        /*
        Aby przygotować własny serwis routingowy pracujący na danych z Open Street Map
        Pobierz wybraną wersję GraphHoppera (.bin.zip, żeby w środku był plik konfiguracyjny) z (w tym wypadku 0.10.0-RC1): 
        https://oss.sonatype.org/content/groups/public/com/graphhopper/graphhopper-web/0.10.0-RC1/
        Zmień nazwę pliku konfiguracyjnego config-example.properties na config.properties
        //Jeżeli nawigacja ma być również dostępna dla pieszych trzeba w pliku ustawień odznaczyć stosowne opcje:
        graph.flag_encoders=car,foot
        Pobierzy wybrany obszar danych np.
        http://download.geofabrik.de/europe/poland/mazowieckie.html
        Uruchom aplikację poleceniem (oczywiście z odpowiednią nazwą pliku .osm.pbf)
        java -jar *.jar jetty.resourcebase=webapp config=config.properties datareader.file=mazowieckie-latest.osm.pbf
        Sprawdź działanie kierując się pod poniższy adres (być może z innymi współrzędnymi odpowiadającymi współrzędnym mapy)
        http://127.0.0.1:8989/route?point=52.2688%2C21.04005&point=52.30123%2C21.03277&locale=pl-PL&instructions=false&vehicle=car&weighting=fastest&elevation=false&points_encoded=false&use_miles=false&layer=Omniscale
        //UWAGA: pierwsze uruchomienie trwa dłużej - GraphHopper buduje bazę możliwych tras przejazdów
        */
        if (routingControl) {
            //techniczna operacja, żeby zawsze była tylko jedna kontrolka z trasą
            routingControl.remove();
        }
        routingControl = L.Routing.control({
            waypoints: [
                e.latlng,
                L.latLng(50.080208574078185,19.92103457450867)
            ],
            routeWhileDragging: true,
            router: L.Routing.graphHopper(undefined /* no api key */, {
                //Jeżeli uruchomimy lokalnie GraphHoppera to będzie domyślnie dostępny pod tym adresem 
                //serviceUrl: 'http://127.0.0.1:8989/route/'
                //uruchomiłem instancję z dostępną Polską pod takim adresem
                serviceUrl: 'http://194.29.178.167:38989/route/'
            })
        }).addTo(mymap);

        console.info(e.latlng);
        var circleRadiusValue =
            document.getElementById('marker-size').value;
        var circleColorValue =
            document.getElementById('marker-color').value;
        var circleLabelValue =
            document.getElementById('marker-label').value;
        var circleMarkerOptions = {
            radius: Number(circleRadiusValue) / 2.0,
            color: circleColorValue
            //inne możliwe opcje stylizacji znacznika
            /*,
            fillOpacity: 1.0,
            fillColor: 'black',
            weight: 1*/
        }

        L.circleMarker(e.latlng, circleMarkerOptions)
            .addTo(this)
            .bindPopup(
                '<h4>' + circleLabelValue + '</h4>' +
                '<p>Dodano mnie o ' + new Date() + '</p>'
            )
            .openPopup();
    });
}

document.addEventListener("DOMContentLoaded", loadMap, false);