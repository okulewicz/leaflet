var mymap;

function MapSettings(controlPrefix, latitudeAlias, longitudeAlias,
    degreesAlias, semisphereAlias, minutesAlias, secondsAlias,
    numericFieldsClass) {
    this.controlPrefix = controlPrefix;
    this.latitudeAlias = latitudeAlias;
    this.longitudeAlias = longitudeAlias;
    this.degreesAlias = degreesAlias;
    this.semisphereAlias = semisphereAlias;
    this.minutesAlias = minutesAlias;
    this.secondsAlias = secondsAlias;
    this.numericFieldsClass = numericFieldsClass;
    this.getLngDecId = function() { return this.controlPrefix + this.longitudeAlias; } ;
    this.getLatDecId = function() { return this.controlPrefix + this.latitudeAlias; } ;
    this.getLngDegId = function() { return this.controlPrefix + this.longitudeAlias + this.degreesAlias; } ;
    this.getLatDegId = function() { return this.controlPrefix + this.latitudeAlias + this.degreesAlias; } ;
    this.getLngSemiId = function() { return this.controlPrefix + this.longitudeAlias + this.semisphereAlias; } ;
    this.getLatSemiId = function() { return this.controlPrefix + this.latitudeAlias + this.semisphereAlias; } ;
    this.getLngMinId = function() { return this.controlPrefix + this.longitudeAlias + this.minutesAlias; } ;
    this.getLatMinId = function() { return this.controlPrefix + this.latitudeAlias + this.minutesAlias; } ;
    this.getLngSecId = function() { return this.controlPrefix + this.longitudeAlias + this.secondsAlias; } ;
    this.getLatSecId = function() { return this.controlPrefix + this.latitudeAlias + this.secondsAlias; } ;
    this.getNumericFieldClass = function() { return this.numericFieldsClass; } ;
}

function onMapMove(event) {
    var lat = this.getCenter().lat;
    var lng = this.getCenter().lng;

    fillInControlValues(lat, lng);

    var numericFields = document.getElementsByClassName(mapSettings.getNumericFieldClass());
    for (var i = 0; i < numericFields.length; ++i) {
        numericFields[i].dispatchEvent(new Event('change'));
    }
}

function onControlValueChange(event) {
    var lat = document.getElementById(mapSettings.getLatDecId()).value;
    var lng = document.getElementById(mapSettings.getLngDecId()).value;
    mymap.off('moveend', onMapMove);
    mymap.panTo({lat: lat, lng: lng});

    fillInControlValues(lat, lng);

    var numericFields = document.getElementsByClassName(mapSettings.getNumericFieldClass());
    for (var i = 0; i < numericFields.length; ++i) {
        numericFields[i].removeEventListener('change', onControlValueChange);
        numericFields[i].dispatchEvent(new Event('change'));
        numericFields[i].addEventListener('change', onControlValueChange);
    }

    mymap.on('moveend', onMapMove);
}

function fillInControlValues(lat, lng) {
    var latSemi = (lat > 0) ? 'N' : 'S';
    var latDeg = Math.floor(Math.abs(lat));
    var latMin = Math.floor((Math.abs(lat) - latDeg) * 60);
    var latSec = Math.round((Math.abs(lat) - latDeg - latMin / 60) * 3600 * 1e2) / 1e2;
    var lngSemi = (lng > 0) ? 'E' : 'W';
    var lngDeg = Math.floor(Math.abs(lng));
    var lngMin = Math.floor((Math.abs(lng) - lngDeg) * 60);
    var lngSec = Math.round((Math.abs(lng) - lngDeg - lngMin / 60) * 3600 * 1e2) / 1e2;
    document.getElementById(mapSettings.getLngDecId()).value = Math.round(lng * 1e4) / 1e4;
    document.getElementById(mapSettings.getLatDecId()).value = Math.round(lat * 1e4) / 1e4;
    document.getElementById(mapSettings.getLngSemiId()).value = lngSemi;
    document.getElementById(mapSettings.getLatSemiId()).value = latSemi;
    document.getElementById(mapSettings.getLngDegId()).value = lngDeg;
    document.getElementById(mapSettings.getLatDegId()).value = latDeg;
    document.getElementById(mapSettings.getLngMinId()).value = lngMin;
    document.getElementById(mapSettings.getLatMinId()).value = latMin;
    document.getElementById(mapSettings.getLngSecId()).value = lngSec;
    document.getElementById(mapSettings.getLatSecId()).value = latSec;
}

function attachEvents(mymap) {
    mymap.on('moveend', onMapMove);
    var numericFields = document.getElementsByClassName(mapSettings.getNumericFieldClass());
    for (var i = 0; i < numericFields.length; ++i) {
        numericFields[i].addEventListener('change', onControlValueChange);
    }
}

function setMapProperties(mymap) {
    mymap.setView([50.05, 19.93], 11);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,\
        <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    }).addTo(mymap);
}

function loadMap() {
    mymap = L.map('js-map-container');
    mapSettings = new MapSettings(
        'map-center-',
        'latitude',
        'longitude',
        '-deg',
        '-semi',
        '-min',
        '-sec',
        'js-numeric-field');
    attachEvents(mymap);
    setMapProperties(mymap);
}

document.addEventListener( "DOMContentLoaded", loadMap, false );